# Monografia TCC

Monografia em desenvolvimento como exigência da disciplina de Projeto e Implementação de Sistemas 1 e 2, 1º e 2º semestre de 2015, Prof. Dr. Nilceu Marana, na Universidade Estadual Paulista "Júlio de Mesquita Filho" - Faculdade de Ciências - Campus Bauru.

Relata o projeto denominado "Desenvolvimento de um Protótipo de Rastreador de Beacons Utilizando o Raspberry Pi", também ainda em desenvolvimento.

Este projeto foi migrado para o [GitHub](https://github.com/gabrielboliveira/Monografia-Peacon).

## Copyright

Este projeto utiliza o [abntex2](https://github.com/abntex/abntex2), e edita alguns pontos como capa e contra-capa para ficar similar ao pedido pelo professor da disciplina. A cópia e edição desse projeto é liberada, somente ficando necessário as devidas referências ao autor: Gabriel Luiz Bastos de Oliveira. 
